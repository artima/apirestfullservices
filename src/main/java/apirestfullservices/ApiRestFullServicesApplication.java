package apirestfullservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestFullServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiRestFullServicesApplication.class, args);
    }

}
